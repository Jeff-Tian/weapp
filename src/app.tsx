import {ApolloProvider} from "@apollo/client";
import {useState} from "react";
import {tryRedirect} from "@/functions/redirect";

import "taro-ui/dist/style/index.scss";
import Taro, {usePageNotFound} from "@tarojs/taro";

import {AppContext, AppNameEnum} from "@/contexts/app-context";
import {RefreshContext} from "@/contexts/refresh-context";

import usePageRefresh from "@/hooks/use-page-refresh";

import {ShareTokenByQueryStringContext} from "@/contexts/share-token";

import "./app.styl";
import {brickverseClient, client} from "./apollo-client";
import MultiProvider from "./components/multi-provider/MultiProvider";
import {
  CurrentClipboardContext,
  CurrentCloudboardContext,
} from "./contexts/clipboard-context";
import {UserInfoContext, UserRolesContext} from "@/contexts/user-info-context";
import {User} from "@authing/guard-react";

if (Taro.getEnv() === Taro.ENV_TYPE.WEAPP) {
  // 以下事件只能在微信小程序端中捕获到
  Taro.onPageNotFound(({isEntryPage, path}) => {
    console.log("path = ", isEntryPage, path);
  });

  Taro.onError((error) => {
    console.error(error);
  });
}

const App = ({children}) => {
  const [appName, setAppName] = useState(AppNameEnum.hardway);
  const {register, unregister, refresh} = usePageRefresh();

  Taro.usePullDownRefresh(() => {
    refresh();
  });

  usePageNotFound(({path}) => {
    tryRedirect(path);
  });

  const [currentClipboard, setCurrentClipboard] = useState("");
  const [currentCloudboard, setCurrentCloudboard] = useState(undefined);
  const [shareTokenByQueryString, setShareTokenByQueryString] = useState(false);
  const [userInfo, setUserInfo] = useState<User | null>(null);
  const [roles, setRoles] = useState<string[]>([]);
  return (
    <MultiProvider
      providers={[
        <AppContext.Provider
          value={{appName, setAppName}}
          key="app-context"
        />,
        <RefreshContext.Provider
          value={{
            refresh,
            register,
            unregister,
          }}
          key="refresh-context"
        />,
        <CurrentClipboardContext.Provider
          value={[currentClipboard, setCurrentClipboard]}
          key="current-clipboard-context"
        />,
        <CurrentCloudboardContext.Provider
          value={[currentCloudboard, setCurrentCloudboard]}
          key="current-cloud-board-context"
        />,
        <ShareTokenByQueryStringContext.Provider
          key="share-token-by-query-string-context"
          value={[shareTokenByQueryString, setShareTokenByQueryString]}
        />,
        <UserInfoContext.Provider
          key="user-info-context"
          value={[
            userInfo,
            setUserInfo
          ]}
        />,
        <UserRolesContext.Provider key="roles-info-context" value={[roles, setRoles]} />,
      ]}
    >
      {appName === AppNameEnum.brickverse ? (
        <ApolloProvider client={brickverseClient}>{children}</ApolloProvider>
      ) : (
        <ApolloProvider client={client}>{children}</ApolloProvider>
      )}
    </MultiProvider>
  );
};

export default App;
