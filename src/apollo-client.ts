import { ApolloClient, ApolloLink, concat, createHttpLink, InMemoryCache, split } from "@apollo/client"
import Taro from "@tarojs/taro"
import crypto from 'crypto'

import { createPersistedQueryLink } from "@apollo/client/link/persisted-queries"
import { createUploadLink } from "apollo-upload-client";
import { setContext } from "@apollo/client/link/context";
import { getToken } from "@/common/token";
import packageJson from "../package.json";

const gatewayGraphQLURl: string = 'https://graph.pa-ca.me'
const brickverseGraphQLURl: string = 'https://strapi.pa-ca.me/graphql'

const platform = Taro.getEnv();
const version = platform === Taro.ENV_TYPE.WEB ? packageJson.version : Taro.getAccountInfoSync().miniProgram?.version;

const theFetch = async (url, options) => {
  Taro.showLoading({ title: `加载中...\n\n${url.toString()}` })

  try {
    const res = await Taro.request({
      url: url.toString(),
      method: (options?.method || 'POST') as 'POST' | 'GET',
      header: {
        'content-type': 'application/json',
        "x-graphql-client-name": `weapp`,
        "x-graphql-client-version": `${platform}-${version}`,
        ...options.headers
      },
      data: options?.body,
      success: console.log,
      timeout: options?.timeout ?? 15000
    })

    return {
      text: async () => {
        if (res.statusCode === 504) {
          await Taro.showToast({
            title: '接口超时，请稍后重试。',
            icon: 'error',
            duration: 3000
          })

          return '';
        }

        try {
          return JSON.stringify(res.data);
        } catch (ex) {
          console.error(ex);

          Taro.showModal({
            title: '解析数据失败',
            content: `${ex.toString()}`
          })

          return '';
        }
      }
    } as any
  } catch (ex) {
    console.error(ex);

    await Taro.showToast({
      title: ex.message,
      icon: 'error',
      duration: 3000
    })

    return '';
  } finally {
    Taro.hideLoading();
  }
}

const httpLink = createHttpLink({
  uri: gatewayGraphQLURl,
  fetch: theFetch
})

const brickverseLink = createHttpLink({
  uri: brickverseGraphQLURl,
  fetch: theFetch
})

const queryLink = createPersistedQueryLink({
  useGETForHashedQueries: true,
  sha256: async (document: string) => crypto.createHash('sha256').update(document).digest('hex')
})

const authLink = setContext(async (_, { headers }) => {
  const token = await getToken();

  console.log('token = ', token);

  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : undefined,
    }
  }
})

const testIfUploadOperation = ({ query }) => {
  const { definitions } = query

  return definitions.some(({ kind, operation, selectionSet: { selections } }) => {
    return kind === 'OperationDefinition' && operation === 'mutation' && selections.some(({ name: { value } }) => value === 'uploadImage')
  })
}

const httpLinkForNormalOperations = ApolloLink.from([queryLink, httpLink]);

const onlineUploadableGraphQL = Taro.getEnv() === Taro.ENV_TYPE.WEB ? 'https://face-swap-itor.onrender.com/graphql' : gatewayGraphQLURl;

const uploadLink = createUploadLink({
  uri: process.env.FACE_SWAP_ENV !== 'local' ? onlineUploadableGraphQL : 'http://localhost:5001/graphql',
  fetch: theFetch
}) as any

export const client = new ApolloClient({
  link: concat(authLink, split(testIfUploadOperation, uploadLink, httpLinkForNormalOperations)),

  cache: new InMemoryCache()
})

const brickverseAuthLink = setContext(async (_, { headers }) => {
  return {
    headers: {
      ...headers,
      Authorization: `Bearer ${process.env.BRICKVERSE_TOKEN}`,
    }
  }
})

export const brickverseClient = new ApolloClient({
  link: concat(brickverseAuthLink, brickverseLink),
  cache: new InMemoryCache()
})

export const noCacheClient = new ApolloClient({
  link: concat(authLink, httpLink),
  cache: new InMemoryCache()
})
