import {gql} from "@apollo/client";
import {client, noCacheClient} from "@/apollo-client";
import Taro from "@tarojs/taro";
import {StorageKeys} from "@/common/constants";

export const GET_PREFERENCE_QUERY = gql`
  query GetPreference($key: String!) {
    myPreference(key: $key) {
      key
      value
    }
  }
`;

export const GET_CLIPBOARD_QUERY = gql`
  query GetClipboard {
    myPreference(key: "${StorageKeys.clipboard}") {
      key
      value
    }
  }
`;

export const getClipboardFromCloud = () => {
  return noCacheClient.query({
    query: GET_CLIPBOARD_QUERY,
    variables: {},
    fetchPolicy: "no-cache",
  });
};

export const getMyZhihuProfile = () => {
  return client.query({
    query: GET_PREFERENCE_QUERY,
    variables: {
      key: StorageKeys.zhihuUserCookie,
    },
  });
};

const SAVE_PREFERENCE_MUTATION = gql`
  mutation SaveMyZhihuCookies($key: String!, $value: String!) {
    saveMyPreference(key: $key, value: $value) {
      key
      value
    }
  }
`;

export const saveMyZhihuAccount = (cookieData: string) => {
  return Promise.all([
    client.mutate({
      mutation: SAVE_PREFERENCE_MUTATION,
      variables: {
        key: StorageKeys.zhihuUserCookie,
        value: cookieData,
      },
    }),
    client.mutate({
      mutation: SAVE_PREFERENCE_MUTATION,
      variables: {
        key: StorageKeys.zhihuUserInfo,
        value: JSON.stringify(Taro.getStorageSync(StorageKeys.zhihuUserInfo)),
      },
    }),
  ]);
};

const SAVE_CLIPBOARD_MUTATION = gql`
  mutation SaveClipboard ($value: String!) {
    saveMyPreference (key: "${StorageKeys.clipboard}", value: $value) {
      key
      value
    }
  }
`;

export const saveClipboardToCloud = (clipboard: string) => {
  return client.mutate({
    mutation: SAVE_CLIPBOARD_MUTATION,
    variables: {
      value: clipboard,
    },
  });
};

const COPY_TO_CLIPBOARD = gql`
  mutation CopyToClipboard($clipboard: ClipboardInput!) {
    copyToClipboard(clipboard: $clipboard) {
      key
      value
    }
  }
`;
export const copyCookieToClipboard = (userId: number, cookieData: string) => {
  saveMyZhihuAccount(cookieData).then(console.log).catch(console.error);

  client
    .mutate({
      mutation: COPY_TO_CLIPBOARD,
      variables: {
        clipboard: {
          key: `zhihu-user-cookie-${userId}`,
          value: JSON.stringify(cookieData),
        },
      },
    })
    .then(console.log)
    .catch(console.error);
};

export const CREATE_RESOURCE = gql`
  mutation CreateResource(
    $code: String!
    $name: String!
    $description: String!
    $actions: [ResourceActionInput]
    $content: String!
    $contentType: String!
  ) {
    createResource(
      code: $code
      name: $name
      description: $description
      namespace: "default"
      actions: $actions
      type: DATA
      content: $content
      contentType: $contentType
    ) {
      code
      name
      description
    }
  }
`;

export const createResource = (name: string) => {
  return noCacheClient
    .mutate({
      mutation: CREATE_RESOURCE,
      variables: {
        name: name,
        code: Math.random().toFixed(10).toString().replace("0.", ""),
        description: name,
        actions: [],
        content: '{}',
        contentType: "application/json",
      },
    })
    .then((res) => {
      console.log(res);

      return res;
    })
    .catch(console.error);
};

export const UPDATE_RESOURCE = gql`
  mutation UpdateResource(
    $code: String!
    $name: String!
    $description: String!
    $actions: [ResourceActionInput]
    $content: String!
    $contentType: String!
  ) {
    updateResource(
      code: $code
      name: $name
      description: $description
      namespace: "default"
      actions: $actions
      type: DATA
      content: $content
      contentType: $contentType
    ) {
      code
      name
      description
    }
  }
`;

export const updateResource = (
  code: string,
  name: string,
  description: string,
  content: string,
  contentType: string,
) => {
  try {
    JSON.parse(content);
  } catch (ex) {
    console.error(ex);

    content = JSON.stringify({text: content});
  }

  return noCacheClient
    .mutate({
      mutation: UPDATE_RESOURCE,
      variables: {
        code: code,
        name: name,
        description: description,
        actions: [],
        content: content,
        contentType: contentType,
      },
    })
    .then((res) => {
      console.log(res);

      return res;
    })
    .catch(console.error);
};

export const LIST_RESOURCES = gql`
  query ListResources {
    resourceList {
      code
      name
      description
    }
  }
`;

export const GET_RESOURCE = gql`
  query GetResource($code: String!) {
    resourceDetail(code: $code) {
      code
      name
      description
      contentType
      content
    }
  }
`;

export const GET_RESOURCE_AUTHORIZED_TARGETS = gql`
  query GetResourceAuthorizedTargets($code: String!) {
    resourceAuthorizedTargets(code: $code) {
      totalCount
      list {
        targetType
        targetIdentifier
      }
    }
  }
`;
