import {gql} from "@apollo/client";

export const GITHUB_STATS = gql`
  query GitHubStats
  {
    viewer {
      login
      contributionsCollection {
        contributionCalendar {
          totalContributions
          weeks {
            contributionDays {
              date
              contributionCount
              color
            }
          }
        }
        totalIssueContributions
        totalCommitContributions
        totalRepositoryContributions
        totalPullRequestContributions
        totalPullRequestReviewContributions
        totalRepositoriesWithContributedIssues
        totalRepositoriesWithContributedCommits
        totalRepositoriesWithContributedPullRequests
        totalRepositoriesWithContributedPullRequestReviews
      }
    }
  }
`

export const GITHUB_TIMELINE = gql`
  query GitHubTimeline {
    viewer {
      contributionsCollection {
        commitContributionsByRepository {
          repository {
            name
          }
          contributions(first: 100) {
            nodes {
              commitCount
              occurredAt
            }
          }
        }
        issueContributionsByRepository {
          repository {
            name
          }
          contributions(first: 100) {
            nodes {
              issue {
                title
                url
                createdAt
              }
            }
          }
        }
        pullRequestContributionsByRepository {
          repository {
            name
          }
          contributions(first: 100) {
            nodes {
              pullRequest {
                title
                url
                createdAt
              }
            }
          }
        }
        pullRequestReviewContributionsByRepository {
          repository {
            name
          }
          contributions(first: 100) {
            nodes {
              pullRequestReview {
                pullRequest {
                  title
                  url
                }
                createdAt
              }
            }
          }
        }
      }
    }
  }
`

export const GITHUB_README = gql`query GetRepositoryReadme($owner: String!, $name: String!) {
  repository(owner: $owner, name: $name) {
    object(expression: "master:README.md") {
      ... on Blob {
        text
      }
    }
  }
}
`;
