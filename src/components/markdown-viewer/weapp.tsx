import {View} from "@tarojs/components"
import remark from "remark";
import remarkHtml from "remark-html";
import unist from 'unist-util-visit';

function imageHandler(baseUrl) {
  return () => (tree) => {
    unist(tree, 'image', (node) => {
      const imageUrl = node.url.startsWith('http') ? node.url : `${baseUrl}${node.url}`;

      node.type = 'html'
      node.value = `<image src="https://uniheart.pa-ca.me/proxy?url=${imageUrl}" mode="widthFix" />`
      return unist.SKIP
    })
  }
}

const WeappMarkdownViewer = ({markdown, baseUrl}) => {
  if (!markdown) {
    return <View>正在加载中……</View>
  }

  const remarked = remark();
  const used = remarked.use(imageHandler(baseUrl)).use(remarkHtml);

  const processed = used.process(markdown.replace(/&nbsp;/g, ' ').replace(/ai/gi, '').replace(/人工智能/g, '').replace(/阿尔法狗/g, '').replace(/学习/g, '解压'));

  return <View dangerouslySetInnerHTML={{__html: processed.contents}}></View>
}

export default WeappMarkdownViewer
