import Taro from "@tarojs/taro";
import {View} from "@tarojs/components";
import WebMarkdownViewer from "./h5";
import WeappMarkdownViewer from "./weapp";
import './markdown-viewer.styl'

const MarkdownViewer = ({markdown, baseUrl}) => {
  return <View className='markdown-viewer'>{Taro.getEnv() === Taro.ENV_TYPE.WEAPP ? <WeappMarkdownViewer markdown={markdown} baseUrl={baseUrl} /> :
    <WebMarkdownViewer markdown={markdown} baseUrl={baseUrl} />}
    </View>
}

export default MarkdownViewer
