import remark from 'remark'
import remarkHtml from "remark-html"
import {View} from "@tarojs/components"
import unist from 'unist-util-visit'

function imageHandler(baseUrl = '') {
  // 由于有一些网站会屏蔽外链，所以需要使用代理
  return () => (tree) => {
    unist(tree, 'image', (node) => {
      const imageUrl = node.url.startsWith('http') ? node.url : `${baseUrl}${node.url}`;

      node.type = 'html'
      node.value = `<img src="https://uniheart.pa-ca.me/proxy?url=${imageUrl}"  alt="${node.alt ?? "图片"}"/>`
      return unist.SKIP
    })
  }
}

const WebMarkdownViewer = ({markdown, baseUrl = ''}) => {
  const remarked = remark();
  const used = remarked.use(imageHandler(baseUrl)).use(remarkHtml);
  const processed = used.process(markdown);

  return <View dangerouslySetInnerHTML={{__html: processed.contents}} />
}

export default WebMarkdownViewer
