import { View } from "@tarojs/components";
import { AtAvatar, AtCard, AtLoadMore } from "taro-ui";
import { fallbackThumbnail } from "@/common/constants";
import Taro from "@tarojs/taro";
import {
  ReactChild,
  ReactFragment,
  ReactPortal,
  useEffect,
  useState,
} from "react";
import { gql, useQuery } from "@apollo/client";
import { ErrorDisplay } from "@/components/errors/ErrorDisplay";
import useComponentRefresh from "@/hooks/use-component-refresh";

import MarkdownViewer from "@/components/markdown-viewer";

import "./blog-list.styl";
import { NoticeBar } from "./notice";

type Blog = {
  id: string | number | undefined;
  slug: string;
  title: string | undefined;
  word_count: any;
  created_at: string | undefined;
  cover: any;
  description:
    | boolean
    | ReactChild
    | ReactFragment
    | ReactPortal
    | null
    | undefined;
};

export const YUQUE_BLOG = gql`
  query PaginatedYuQue($skip: Float!, $take: Float!) {
    paginatedYuque(skip: $skip, take: $take) {
      id
      title
      description
      word_count
      created_at
      cover
      slug
      status
    }
  }
`;

let noticeArticle = {
  id: "notice",
  title: "郑重声明，本页仅分享计算机科学与编程技术，以供需要的人进行学习参考。",
  description:
    "哈德韦是我本人的网名，业余时间喜欢写一些技术文章，以供初学者进行学习。在小程序诞生后，作为技术人，不免非常感兴趣，特此制作本小程序，用来展示自己的技术文章。但是不提供任何信息资讯服务，谢谢理解！",
  created_at: "2021-10-15",
  cover: fallbackThumbnail,
  slug: "notice",
  status: "1",
} as unknown as Blog;

noticeArticle.word_count = String(noticeArticle.description).length;

export const BlogList = () => {
  Taro.useReachBottom(() => {
    console.log("用户上拉加载更多……");
    handleClick();
  });

  const [blogs, setBlogs] = useState<any>([]);
  const [skip, setSkip] = useState(0);
  const [take] = useState(5);
  const { loading, error, data, refetch } = useQuery(YUQUE_BLOG, {
    variables: { skip: 0, take: 5 },
  });
  useComponentRefresh(refetch);
  const [status, setStatus]: [
    "more" | "loading" | "noMore" | undefined,
    Function,
  ] = useState("loading");

  const handleClick = () => {
    setSkip(skip + take);
    setStatus("loading");
    refetch({ skip: skip + take, take }).then();
  };

  useEffect(() => {
    if (data) {
      setStatus("more");

      setBlogs(blogs.concat(data.paginatedYuque));
    }
  }, [data]);

  if (error) {
    console.error(error);
    return (
      <View>
        <ErrorDisplay error={error}>
          <button onClick={() => refetch()}>重试</button>
        </ErrorDisplay>
      </View>
    );
  }

  if (new Date().toISOString() < "2024-10-20") {
    return <View></View>;
    return (
      <View>
        <NoticeBar />
        {blogs
          .filter(
            (x: Blog) =>
              [
                "notice",
                "dq01zxe0hpnqhhkv",
                "cc77w4uhlbq1a1ik",
                "avgogu2ut93e4a5r",
                "wfe8mm3zebwk8i2u",
                "eul1hh5zb2ol2xvd",
                "bpikn16y2hfybscu",
                "ad2ha2vntysmw8f6",
                "fck4b753h8naomw9",
                "gk7cvigvzeywi6le",
                "wg5372vtd7oflhn5",
                "odv4x3wd7lyw558p",
                "ykdopf3egpconrtk",
                "string",
                "ur5tty1p3xndystn",
                "lasv66mm5axeppxe",
                "tknly5ng1298razc",
                "ds83az0vxuavgyoi",
                "iyfvo618ssbch7ix",
                "uf8ggyfazt5u41xp",
                "lgrnwbau9hq4ighx",
                "fiisg1molfi17zrc",
                "skx7mstwkqz9zwz4",
                "vy7af38ydcn5qxbl",
                "kpotk95b1vn9uzu3",
                "sgyok5",
                "dguhht9fp9onm0x7",
                "eqh3cw2t5iw4g15i",
                "fck4b753h8naomw9",
                "otm8g3",
                "gk7cvigvzeywi6le",
                "idlkas",
                "te662r",
                "ncy6qqxazskd5voh",
                "tga1all2db1qdiox",
                "vvdmqy3wopho0z95",
                "lc5bhykdziiyufk5",
                "qmg3i0ph2wnsut0p",
                "zh3gv2m2gr0zk328",
                "llbmzruepagdxag5",
                "glzasd54e43alfpg",
                "tn6bk2yuieivmg4x",
                "ltdlnkcecvttt18f",
                "qdgnygenlm3r1gyo",
                "mywzgoe9wffotk50",
                "xt2v803qgugcxxqh",
                "wsehy3sgpl83ltro",
                "zvkluxrw8ahwca65",
                "uwm6ffs4fqtl9byy",
                "dr6bhyi25ei95gw0",
                "inqtsena84spksyi",
                "of92y1lho7pd2nax",
                "rr8mo6g948hgo1ni",
                "cbprn8gt8umwshli",
                "ka6wugb3kz04t2gb",
                "mn5ovrt2olqyolv4",
                "rsamx22lxfa036uq",
                "fg3fl7rfp97syxg3",
                "qt172mokds1mkuff",
                "roez38wngtuk1gkg",
                "tr37p2",
                "ar5rxw",
                "nr7ce3",
                "grggdg",
                "gb1ui4",
                "ns3r12",
                "pg4ir4",
                "mbh5uc",
                "fxnsuw",
                "vvkl0c",
              ].indexOf(x.slug) >= 0,
          )
          .map((article: Blog, index) => (
            <View key={article.id + "-" + index}>
              <AtCard
                title={article.title}
                extra={`${article.word_count} 字`}
                note={`哈德韦写于 ${article.created_at}。纯技术文章，非信息资讯`}
                thumb={
                  article.cover
                    ? `https://uniheart.pa-ca.me/proxy?url=${article.cover}`
                    : fallbackThumbnail
                }
                onClick={() =>
                  article.id !== "notice" &&
                  Taro.navigateTo({
                    url: `/pages/yuque/article?id=${article.id}`,
                  })
                }
              >
                <AtAvatar
                  image={
                    article.cover
                      ? `https://uniheart.pa-ca.me/proxy?url=${article.cover}`
                      : fallbackThumbnail
                  }
                  size="large"
                />
                <MarkdownViewer markdown={article.description} baseUrl="" />
              </AtCard>
            </View>
          ))}
      </View>
    );
  }

  return (
    <View>
      <NoticeBar />
      {blogs
        .filter((x) => x.status === "1")
        .map((article: Blog, index) => (
          <View key={article.id + "-" + index}>
            <AtCard
              title={article.title}
              extra={`${article.word_count} 字`}
              note={`哈德韦写于 ${article.created_at}。纯技术文章，非信息资讯`}
              thumb={
                article.cover
                  ? `https://uniheart.pa-ca.me/proxy?url=${article.cover}`
                  : fallbackThumbnail
              }
              onClick={() =>
                article.id !== "notice" &&
                Taro.navigateTo({
                  url: `/pages/yuque/article?id=${article.id}`,
                })
              }
            >
              <AtAvatar
                image={
                  article.cover
                    ? `https://uniheart.pa-ca.me/proxy?url=${article.cover}`
                    : fallbackThumbnail
                }
                size="large"
              />
              <MarkdownViewer markdown={article.description} baseUrl="" />
            </AtCard>
          </View>
        ))}
      <AtLoadMore
        loadingText="加载中……"
        onClick={handleClick.bind(this)}
        status={loading ? "loading" : status}
      />
    </View>
  );
};
