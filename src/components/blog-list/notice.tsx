import { fallbackThumbnail } from "@/common/constants"
import { View } from "@tarojs/components"
import Taro from "@tarojs/taro"
import {AtAvatar, AtCard, AtNoticebar} from "taro-ui"

export const NoticeBar = () => <AtNoticebar icon="volume-plus">
  郑重声明：以下是“哈德韦”写的技术文章，与信息资讯无关，仅供大家学习参考。
</AtNoticebar>

export const NoticeCard = () => {
    return <AtCard
      title='郑重声明，本小程序仅展示个人技术博客文章'
      note='不提供信息资讯服务'
      extra='哈德韦'
      thumb={fallbackThumbnail}
      onClick={() => {
            Taro.showModal({
                title: '郑重声明，本小程序仅展示个人技术博客文章',
                content: '哈德韦是我本人的网名，业余时间喜欢写写博客文章。在小程序诞生后，作为技术人，不免非常感兴趣，特此制作本小程序，用来展示自己的技术博客文章。但是不提供任何信息资讯服务，谢谢理解！',
                confirmText: '我理解了',
                showCancel: false
            });
        }}
    >
        <AtAvatar image={fallbackThumbnail} size='large'></AtAvatar>
        <NoticeDetail />
    </AtCard>
}

export const NoticeDetail = () => {
    return <View className='at-article'>
        <View className='at-article__h1'>
            郑重声明，本小程序仅展示个人技术博客文章
        </View>
        <View className='at-article__info'>
            不提供信息资讯服务
        </View>
        <View className='at-article__content'>
            <View className='at-article__section'>
                <View className='at-article__p'>
                    哈德韦是我本人的网名，业余时间喜欢写写博客文章。在小程序诞生后，作为技术人，
                    不免非常感兴趣，特此制作本小程序，用来展示自己的技术博客文章。但是不提供任何
                    信息资讯服务，谢谢理解！
                </View>
            </View>
        </View>
    </View>
}
