import CalendarHeatMap from "@/components/calendar-heat-map";
import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {GITHUB_STATS} from "@/api/github";

const GitHubCalendar = ({data}) => {
  const contributions = data?.viewer?.contributionsCollection?.contributionCalendar?.weeks?.map((week: { contributionDays: any[]; }) =>
    week.contributionDays.map(day => ({
      date: day.date,
      count: day.contributionCount
    }))
  ).flat() ?? [];

  return <CalendarHeatMap data={contributions} showMonthLabel showWeekLabel theme='github' />
}

export default withGraphqlQuery(GitHubCalendar, GITHUB_STATS);
