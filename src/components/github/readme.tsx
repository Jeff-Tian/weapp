import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {GITHUB_README} from "@/api/github";
import MarkdownViewer from "@/components/markdown-viewer";

const ReadMe = ({data}) => {
  return <MarkdownViewer markdown={data?.repository?.object?.text} baseUrl='https://raw.githubusercontent.com/Jeff-Tian/tic-tac-toe-ai/master/' />
}

export default withGraphqlQuery(ReadMe, GITHUB_README, {variables: {owner: 'Jeff-Tian', name: 'tic-tac-toe-ai'}});
