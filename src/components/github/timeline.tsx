import {View} from "@tarojs/components";
import {AtTimeline} from "taro-ui";
import {GITHUB_TIMELINE} from "@/api/github";
import {Item} from "taro-ui/types/timeline";
import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";

const GitHubTimeline = ({data}) => {
  const {
    commitContributionsByRepository,
    issueContributionsByRepository,
    pullRequestContributionsByRepository,
    pullRequestReviewContributionsByRepository
  } = data?.viewer?.contributionsCollection ?? {};

  const items: Array<Item & { occurredAt: string }> = [];

  commitContributionsByRepository?.forEach(({repository, contributions}) => {
    contributions.nodes.filter(x => !!x).forEach(({commitCount, occurredAt}) => {
      items.push({
        title: `在 ${repository.name} 提交了 ${commitCount} 次`,
        content: [occurredAt],
        icon: 'clock',
        occurredAt: occurredAt
      })
    })
  })

  issueContributionsByRepository?.forEach(({repository, contributions}) => {
    try {
      contributions.nodes.filter(x => !!x).forEach(({issue}) => {
        if (issue) {
          items.push({
            title: `在 ${repository.name} 提交了问题：${issue.title}`,
            content: [issue.createdAt],
            icon: 'clock',
            occurredAt: issue.createdAt
          })
        }
      })
    } catch (ex) {
      console.error(ex);
      debugger;
    }
  })

  pullRequestContributionsByRepository?.forEach(({repository, contributions}) => {
    contributions.nodes.filter(x => !!x).forEach(({pullRequest}) => {
      items.push({
        title: `在 ${repository.name} 提交了拉取请求：${pullRequest.title}`,
        content: [pullRequest.createdAt],
        icon: 'clock',
        occurredAt: pullRequest.createdAt
      })
    })
  })

  pullRequestReviewContributionsByRepository?.forEach(({repository, contributions}) => {
    contributions.nodes.filter(x => !!x).forEach(({pullRequestReview}) => {
      items.push({
        title: `在 ${repository.name} 提交了拉取请求审查：${pullRequestReview.pullRequest.title}`,
        content: [pullRequestReview.createdAt],
        icon: 'clock',
        occurredAt: pullRequestReview.createdAt
      })
    })
  })

  items.sort((a, b) => new Date(b.occurredAt).getTime() - new Date(a.occurredAt).getTime())

  return (
    <View>
      <AtTimeline
        pending
        items={items}
      >
      </AtTimeline>
    </View>
  )
}

export default withGraphqlQuery(GitHubTimeline, GITHUB_TIMELINE);
