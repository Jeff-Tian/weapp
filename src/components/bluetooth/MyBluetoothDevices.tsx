import {View} from "@tarojs/components";
import Taro from "@tarojs/taro";
import {useEffect, useState} from "react";
import JsonViewer from "@/components/rendered-viewers/JsonViewer";

// ArrayBuffer转16进度字符串示例
function ab2hex(buffer) {
  var hexArr = Array.prototype.map.call(
    new Uint8Array(buffer),
    function (bit) {
      return ('00' + bit.toString(16)).slice(-2)
    }
  )
  return hexArr.join('');
}


const MyBluetoothDevices = () => {
  const [devices, setDevices] = useState([])

  useEffect(() => {
    Taro.openBluetoothAdapter({
      success: (res) => {
        console.log(res);
      },
      fail: (err) => {
        console.error(err);
      },
      complete: (res) => {
        console.log(res);

        Taro.getBluetoothDevices({
          success: (res) => {
            console.log(res.devices);
            setDevices(res.devices);
          },
          fail: (err) => {
            console.error(err);
            setDevices([]);
          }
        });
      }
    });

    return () => {
      Taro.closeBluetoothAdapter({
        success: (res) => {
          console.log('closeBluetoothAdapter success', res);
        },
        fail: (err) => {
          console.error('closeBluetoothAdapter fail', err);
        }
      })
    }
  }, []);

  if (!devices || devices.length === 0) {
    return null;
  }

  return <View>
    <View>我已连接的蓝牙设备：</View>
    {
      (devices || []).map((device, index) => {
        return <View key={index}>
          <View>设备名称：{device.name}</View>
          <View>设备ID：{device.deviceId}</View>
          <View>设备信号强度：{device.RSSI}</View>
          <View>设备广播数据：{ab2hex(device.advertisData)}</View>
          <View>更多信息：</View>
          <JsonViewer json={device} />
        </View>
      })
    }
  </View>
}

export default MyBluetoothDevices;
