import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import Taro from "@tarojs/taro";
import {Image, View} from "@tarojs/components";
import WebMarkdownViewer from "@/components/markdown-viewer/h5";
import {useEffect, useState} from "react";
import remark from "remark";
import remarkHtml from "remark-html";
import './article.styl'

if (process.env.TARO_ENV !== 'h5') {
  require('@tarojs/taro/html.css')


  Taro.options.html && (Taro.options.html.transformElement = (el) => {
    if (el.nodeName === 'image') {
      el.setAttribute('mode', 'widthFix')
    }

    return el
  })
}

const YuqueArticle = ({data, action}) => {
  const [html, setHtml] = useState(data?.yuque?.body_html || '')

  useEffect(() => {
    if (data && data.yuque && data.yuque.body) {
      if (Taro.getEnv() === Taro.ENV_TYPE.WEB) {
        action({
          html: html,
          markdown: data.yuque.body
        })

        return;
      }

      const remarked = remark();
      const used = remarked.use(remarkHtml);
      const processed = used.process(data.yuque.body);

      setHtml(String(processed))

      action({
        html: html,
        markdown: data.yuque.body
      })
    }
  }, [data])

  return <View className='at-article'>
    <Image
      className='at-article__img'
      src={`https://uniheart.pa-ca.me/proxy?url=${data?.yuque?.cover || 'https://pica.zhimg.com/v2-7269402d55c437cb6857c2747e352b34_1440w.jpg?source=32738c0c'}`}
      mode='widthFix'
    />

    <View className='at-article__h1'>
      {data?.yuque?.title ?? '精彩内容即将呈现……'}
    </View>

    <View className='at-article__info'>
      {data?.yuque?.created_at}&nbsp;&nbsp;&nbsp;{data?.yuque?.word_count} 字
    </View>

    <View className='at-article__content taro_html'>
      <View className='at-article__section'>
        {
          Taro.getEnv() === Taro.ENV_TYPE.WEB ?
            <WebMarkdownViewer markdown={data?.yuque?.body ?? '一大波文字正在来袭……'} /> :
            <View dangerouslySetInnerHTML={{__html: html ?? '一大波文字正在来袭……'}} />
        }
      </View>
    </View>
  </View>
}

export default withGraphqlQuery(YuqueArticle);
