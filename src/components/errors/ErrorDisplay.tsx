import {Button, View} from "@tarojs/components";
import * as util from "util";
import {DocumentNode} from "@apollo/client";
import './errors.styl';

export const ErrorDisplay = ({error, children, refetch, query}: {
  error: object,
  children: any,
  refetch?: any,
  query?: DocumentNode
}) => {
  console.log('refetch = ', refetch);

  return <View
    className='at-article'
  >
    <View className='at-article__h1'>对不起！发生了错误……</View>
    <View className='at-article__p'>有可能内容被删除了，如果重试无效，请返回查看其他内容。</View>
    <View className='at-article__p'>{util.inspect(error)}</View>
    <View className='at-article__content'>
      {refetch && <Button onClick={refetch}>重试</Button>}
      <View>{children}</View>
      <View>涉及到的查询是：</View>
      <View>{JSON.stringify(query)}</View>
    </View>
  </View>;
}
