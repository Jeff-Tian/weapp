import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {View, Text} from "@tarojs/components";
import {GET_RESOURCE, updateResource} from "@/api/user-service";
import {useContext, useEffect, useState} from "react";
import {AtButton, AtTextarea} from "taro-ui";
import Taro, {useShareAppMessage} from "@tarojs/taro";
import {URLSearchParams} from "@tarojs/runtime";
import {gql} from "@apollo/client";
import {client} from "@/apollo-client";
import {RefreshContext} from "@/contexts/refresh-context";
import {getCurrentPagePathAndQueryString} from "@/functions/current-page";
import {getResourceNameWithoutOwnerUserIdPrefix} from "@/common/resources";
import {RenderedViewer} from "@/components/rendered-viewers";

const RequestAccess = ({resourceCode}) => {
  const {refresh} = useContext(RefreshContext);

  const setupAuthorizeOnBehalf = async (ownerShareToken: string) => {
    const result = await client.mutate({
      mutation: gql`
        mutation authorizeResourceOnBehalf(
          $resourceCode: String!
          $ownerToken: String!
          $actions: [ResourceActionInput!]
        ) {
          authorizeResourceOnBehalf(
            code: $resourceCode
            ownerToken: $ownerToken
            actions: $actions
          ) {
            success
          }
        }
      `,
      variables: {
        resourceCode: resourceCode,
        ownerToken: ownerShareToken,
        actions: [{name: `read`, description: `read`}],
      },
    });

    refresh();

    await Taro.showToast({
      title: result.data.authorizeResourceOnBehalf.success
        ? "授权成功"
        : "授权失败",
      icon: result.data.authorizeResourceOnBehalf.success ? "success" : "error",
      duration: 2000,
    });
  };

  // 从 query string 或者 hash 中获取分享 token
  let shareToken = Taro.getCurrentInstance().router?.params?.token;

  if (!shareToken) {
    shareToken = new URLSearchParams(window.location.hash.substr(1)).get(
      "token",
    );
  }

  useEffect(() => {
    if (shareToken) {
      setupAuthorizeOnBehalf(shareToken).then(() => {
        console.log("授权完成");
      });
    }
  }, [shareToken]);

  if (shareToken) {
    return <View>自动授权中……</View>;
  }

  return <View>你需要申请权限</View>;
};

const ResourceDetail = ({data}) => {
  const [sharePath, setSharePath] = useState("");

  useEffect(() => {
    getCurrentPagePathAndQueryString(
      Taro.getCurrentInstance().router,
      true,
    ).then((path) => {
      setSharePath(path);
    });
  }, []);

  useShareAppMessage(() => {
    return {
      path: sharePath,
    };
  });

  const [editView, setEditView] = useState(false);
  const [content, setContent] = useState(data?.resourceDetail?.content);

  return editView ? (
    <View>
      <View
        onClick={() =>
          Taro.showToast({
            title: data?.resourceDetail?.contentType,
            icon: "none",
            duration: 2000,
          })
        }
      >
        {getResourceNameWithoutOwnerUserIdPrefix(data?.resourceDetail?.name)}
      </View>
      <View>{data?.resourceDetail?.description}</View>
      <View>{data?.resourceDetail?.contentType}</View>
      <AtTextarea
        maxLength={100000}
        value={content}
        onChange={(value) => setContent(value)}
      >
        {data?.resourceDetail?.content}
      </AtTextarea>
      <View className="at-row">
        <View className="at-col">
          <AtButton
            type="secondary"
            onClick={(event) => {
              event.preventDefault();
              event.stopPropagation();

              setEditView(false);
            }}
            size="small"
          >
            取消
          </AtButton>
        </View>
        <View className="at-col"></View>
        <View className="at-col">
          <AtButton
            size="small"
            type="primary"
            onClick={() =>
              updateResource(
                data?.resourceDetail?.code,
                data?.resourceDetail?.name,
                data?.resourceDetail.description,
                content,
                data?.resourceDetail?.contentType,
              ).then(() => {
                setEditView(false);
              })
            }
          >
            保存
          </AtButton>
        </View>
      </View>
    </View>
  ) : (
    <View>
      <View
        onClick={() =>
          Taro.showToast({
            title: data?.resourceDetail?.contentType,
            icon: "none",
            duration: 2000,
          })
        }
      >
        {getResourceNameWithoutOwnerUserIdPrefix(data?.resourceDetail?.name)}
      </View>
      <View>{content ?? data?.resourceDetail?.description}</View>

      <RenderedViewer content={content}/>

      <View onClick={() => setEditView(true)}>
        {!!content
          ? content + '（点击此处即可修改内容）'
          : "还没有填写内容，点击此处即可填写或者修改内容"}
      </View>
    </View>
  );
};

const ResourceDetailWithCode = withGraphqlQuery(ResourceDetail);

export default ({code}) => (
  <ResourceDetailWithCode
    graphqlQuery={GET_RESOURCE}
    graphqlQueryOptions={{variables: {code}}}
    ErrorComponent={() => (
      <View>
        <Text>你还没有权限查看本资源</Text>
        <RequestAccess resourceCode={code}/>
      </View>
    )}
  />
);
