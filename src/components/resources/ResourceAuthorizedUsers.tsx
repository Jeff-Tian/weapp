import {View} from "@tarojs/components";
import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {GET_RESOURCE_AUTHORIZED_TARGETS} from "@/api/user-service";
import {AtAvatar, AtIcon} from "taro-ui";
import {useContext, useEffect} from "react";
import {getUserInfo} from "@/common/login";
import {trustedUserIds} from "@/components/higher-orders/with-protected";
import {UserInfoContext} from "@/contexts/user-info-context";

const ResourceAuthorizedUsers = ({data, extra}) => {
  const [userInfo, setUserInfo] = useContext(UserInfoContext);
  useEffect(() => {
    getUserInfo().then(setUserInfo);
  }, [data]);

  return (
    <View>
      <View>谁可以访问？</View>
      {[
        {targetIdentifier: "你自己"},
        ...(data?.resourceAuthorizedTargets?.list ?? []),
      ].map((user) => {
        return (
          <View
            style={{display: "inline-block", verticalAlign: "middle"}}
            key={`authorized-user-${user.targetIdentifier}-${Math.random()}`}
          >
            <AtAvatar text={user.targetIdentifier} circle></AtAvatar>
          </View>
        );
      })}
      {!!userInfo && trustedUserIds.indexOf(userInfo.id) && (
        <View style={{display: "inline-block"}}>
          <AtIcon
            value="add-circle"
            onClick={() => extra.setIsUserListOpen(true)}
            size={64}
          />
        </View>
      )}
    </View>
  );
};

const ResourceAuthorizedUsersByResourceCode = withGraphqlQuery(
  ResourceAuthorizedUsers,
);

export default ({resourceCode, setIsUserListOpen}) => (
  <ResourceAuthorizedUsersByResourceCode
    graphqlQuery={GET_RESOURCE_AUTHORIZED_TARGETS}
    graphqlQueryOptions={{variables: {code: resourceCode}}}
    extra={{setIsUserListOpen}}
  />
);
