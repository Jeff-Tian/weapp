import {View} from "@tarojs/components";
import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {LIST_RESOURCES} from "@/api/user-service";
import {setupNavigate} from "@/functions/redirect";
import {noCacheClient} from "@/apollo-client";
import {AtList, AtListItem} from "taro-ui";
import {getResourceNameWithoutOwnerUserIdPrefix} from "@/common/resources";

const ResourceList = ({data}) => {
  return (
    <View>
      <View>备忘录列表</View>

      <AtList>
        {data?.resourceList.map((resource) => (
          <AtListItem
            onClick={setupNavigate(
              `/pages/subpages/auth/resource?code=${resource.code}`,
            )}
            key={resource.code}
            title={getResourceNameWithoutOwnerUserIdPrefix(resource.name)}
            note={resource.description}
          ></AtListItem>
        ))}
      </AtList>
    </View>
  );
};

export default withGraphqlQuery(ResourceList, LIST_RESOURCES, {
  fetchPolicy: "no-cache",
  client: noCacheClient,
});
