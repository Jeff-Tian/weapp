import { View } from "@tarojs/components";
import LinkedImage from "@/components/LinkedImage";

export const Wallpapers = () => {
  return (
    <View>
      <View>我制作的手机壁纸</View>
      <LinkedImage src="https://a.l3n.co/i/45u3Pk.png" />
      <LinkedImage src="https://b.l3n.co/i/45us4e.png" />
      <LinkedImage src="https://c.l3n.co/i/45umra.png" />
      <LinkedImage src="https://b.l3n.co/i/45udlM.png" />
      <LinkedImage src="https://b.l3n.co/i/RxEd2z.jpeg" />
    </View>
  );
};
