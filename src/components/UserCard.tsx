import {User} from "@authing/guard-react";
import {AtCard} from "taro-ui";
import {View} from "@tarojs/components";
import Taro from "@tarojs/taro";
import {RoleTags} from "@/components/roles/roleTags";

export const UserCard = ({userInfo}: { userInfo: User | undefined }) => {
  const navigateToProfile = () => {
    const currentPath = Taro.getCurrentInstance().router?.path;

    if (currentPath?.startsWith('/pages/subpages/auth/profile') && Taro.getEnv() === Taro.ENV_TYPE.WEB) {
      window.open('https://www.brickverse.net/profile')
      return;
    }

    if (userInfo?.id === '6204de0a49c31874b97d470b' || userInfo?.id === '6244f2c0016cd83f2177f941' || Taro.getEnv() === Taro.ENV_TYPE.WEB) {
      return Taro.navigateTo({url: '/pages/subpages/auth/profile'});
    } else {
      Taro.showModal({
        title: `仅网页版可用`,
        content: `请在网页版中查看个人信息。（${userInfo?.id}）`,
        showCancel: false,
        confirmText: '知道了',
        confirmColor: '#1890ff',
        complete: () => {
          Taro.setClipboardData({data: userInfo?.id || ''})
        }
      })
    }
  };

  return userInfo ?
    <AtCard note={`第一次见到你是在 ${userInfo.createdAt}，上次是在 ${userInfo.lastLogin} 见过你哦`}
      extra={userInfo.phone || undefined}
      title={'很高兴见到你，' + (userInfo.username || userInfo.nickname || userInfo.email || undefined) + '！'}
      thumb={userInfo.photo || undefined}
      onClick={navigateToProfile}
    >
      你一共来过 {userInfo.loginsCount} 次。
      <RoleTags />
    </AtCard> : <View>未获取到信息</View>
}
