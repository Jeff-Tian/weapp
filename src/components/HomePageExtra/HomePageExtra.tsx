import { Image, Swiper, SwiperItem } from "@tarojs/components";
import StackOverflowWrapper from "@/components/StackOverflowWrapper";
import Taro from "@tarojs/taro";
import LinkedInWrapper from "@/components/LinkedInWrapper";
import GitHubCalendar from "@/components/github/calendar";
import './banner.styl';

export const Banner = () => <Swiper
  indicatorColor='#999'
  indicatorActiveColor='#333'
  circular
  indicatorDots
  autoplay
  full
  displayMultipleItems={1}
  className='banner'
>
  <SwiperItem className='banner-item'
    onClick={() => Taro.navigateTo({ url: `/pages/subpages/react-view/webview?src=${encodeURIComponent('https://mp.weixin.qq.com/s/nwUTbYfi7SMh2X5Lcn-YpA')}` })}
  >
    <Image
      src={`https://uniheart.pa-ca.me/proxy?url=${encodeURIComponent('https://raw.githubusercontent.com/Jeff-Tian/wechat-oauth-ts/master/img_1.png')}`}
      mode='widthFix'
    />
  </SwiperItem>
  <SwiperItem className='banner-item' onClick={() => Taro.navigateTo({ url: '/pages/subpages/tictactoe/ai' })}>
    <Image
      src='https://repository-images.githubusercontent.com/112730526/90ecaf80-b2eb-11e9-9c91-5cd6607772da'
      mode='widthFix'
    />
  </SwiperItem>
  <SwiperItem className='banner-item'>
    <StackOverflowWrapper />
  </SwiperItem>
  <SwiperItem className='banner-item'>
    <LinkedInWrapper />
  </SwiperItem>
  <SwiperItem className='banner-item' onClick={() => Taro.navigateTo({ url: `/pages/subpages/about/github` })}>
    <GitHubCalendar />
  </SwiperItem>
</Swiper>

