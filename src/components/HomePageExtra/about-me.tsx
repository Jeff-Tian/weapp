import GITHUB_README from "@/components/github/readme";

export const AboutMe = () => {
  return <GITHUB_README graphqlQueryOptions={{variables: {owner: 'Jeff-Tian', name: 'Jeff-Tian'}}}/>
}
