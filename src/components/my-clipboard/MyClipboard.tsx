import { View } from "@tarojs/components";
import { AtButton, AtDivider, AtIcon } from "taro-ui";
import CurrentClipBoard from "@/components/my-clipboard/CurrentClipBoard";
import { getClipboardFromCloud, saveClipboardToCloud } from "@/api/user-service";
import Taro from "@tarojs/taro";
import { useContext } from "react";
import { CurrentClipboardContext, CurrentCloudboardContext } from "@/contexts/clipboard-context";
import CurrentCloudBoard from "@/components/my-clipboard/CurrentCloudBoard";

const MyClipboard = () => {

  const saveToCloud = async () => {
    const currentClipboardData = await Taro.getClipboardData();
    await saveClipboardToCloud(currentClipboardData.data);
    setCurrentCloudboard({ myPreference: { value: currentClipboardData.data } });
  }

  const getFromCloud = async () => {
    const clipboardData = await getClipboardFromCloud();
    Taro.setClipboardData({
      data: clipboardData.data.myPreference.value
    })

    setCurrentClipboard(clipboardData.data.myPreference.value)
  }

  const [currentClipboard, setCurrentClipboard] = useContext(CurrentClipboardContext);
  const [currentCloudboard, setCurrentCloudboard] = useContext(CurrentCloudboardContext);

  return <View>
    <AtDivider></AtDivider>
    <View style={{ textAlign: 'center' }}>
      <AtIcon value='iphone-x' size='20' color='blue' onClick={()=>{
        Taro.setClipboardData({data: currentClipboard, success: ()=>{
          Taro.showToast({title: '已将本地内容复制到系统剪贴板', icon: 'success', duration: 2000});
          }});
      }}
      />
      我的云剪贴板
    </View>
    <View className='at-row'>
      <View className='at-col' style={{ paddingRight: '10px' }}>当前设备</View>
      <View className='at-col' style={{ paddingLeft: '10px' }}>云端</View>
    </View>
    <View className='at-row'>
      <View className='at-col' style={{ paddingRight: '10px' }}>
        <CurrentClipBoard />
      </View>
      <View className='at-col' style={{ paddingLeft: '10px' }}>
        <CurrentCloudBoard data={currentCloudboard} />
      </View>
    </View>
    <View className='at-row'>
      <View className='at-col' style={{ paddingRight: '10px' }}>
        <AtButton type='primary' onClick={() => saveToCloud()}>
          <AtIcon value='upload' size='20' color='#fff' />
          同步到云端
          <AtIcon value='arrow-right' size='20' color='#fff' />
        </AtButton>
      </View>
      <View className='at-col' style={{ paddingLeft: '10px' }}>
        <AtButton type='secondary' circle onClick={() => getFromCloud()}>
          <AtIcon value='arrow-left' size='20' color='#000' />
          从云端获取
          <AtIcon value='download' size='20' color='#000' />
        </AtButton>
      </View>
    </View>
    <AtDivider />
  </View>
}

export default MyClipboard;
