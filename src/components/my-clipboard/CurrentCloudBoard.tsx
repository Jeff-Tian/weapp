import {View} from "@tarojs/components";
import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {GET_CLIPBOARD_QUERY} from "@/api/user-service";
import {noCacheClient} from "@/apollo-client";

const CurrentCloudBoard = ({data}) => {
  console.log('data of cloud board = ', data)
  return <View>{data?.myPreference?.value}</View>
}

export default withGraphqlQuery(CurrentCloudBoard, GET_CLIPBOARD_QUERY, {
  fetchPolicy: 'no-cache',
  client: noCacheClient,
});
