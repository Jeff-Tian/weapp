import {useContext, useEffect} from "react";
import {View} from "@tarojs/components";
import Taro from "@tarojs/taro";
import { CurrentClipboardContext } from "@/contexts/clipboard-context";

const CurrentClipBoard = () => {
  const [clipboard, setClipboard] = useContext(CurrentClipboardContext);

  useEffect(() => {
    Taro.getClipboardData({
      success: res => {
        console.log("res = ", res);
        setClipboard(res.data)
      },
      fail: err => {
        console.error("err = ", err);
      }
    })
  }, []);

  return <View onClick={async ()=>{
    if(Taro.getEnv() === Taro.ENV_TYPE.WEB){
      const clipboardData = await navigator.clipboard.readText();

      Taro.setClipboardData({
        data: clipboardData
      })

      setClipboard(clipboardData);
    }
  }}
  >{clipboard}</View>
}

export default CurrentClipBoard;
