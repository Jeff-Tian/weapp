import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {View} from "@tarojs/components";
import {AtTag} from "taro-ui";
import Taro from "@tarojs/taro";
import {gql} from "@apollo/client";
import {UserRolesContext} from "@/contexts/user-info-context";
import {useContext} from "react";

export const RoleTags = withGraphqlQuery(
  ({data}) => {
    const [_, setRoles] = useContext(UserRolesContext);

    setRoles(data?.myRoleList ?? []);

    return <View>{data?.myRoleList?.map(role => {
      return <AtTag key={`role-tag-${role.code}`} type='primary' active={role.code === 'wmp-trusted-users'} circle
                    onClick={(_tagInfo, event) => {
                      event.preventDefault();
                      event.stopPropagation();

                      Taro.showToast({
                        title: role.description ?? '',
                        icon: 'none',
                        duration: 5000
                      });
                    }}
      >{role.code}</AtTag>
    })}</View>
  },
  gql`{
    myRoleList (namespace: "6389cf494b2e2b25f818ab6d") {
      code
      name
      description
      namespace
      namespaceName
      status
      disableTime
    }
  }`)
