import Taro, {ENV_TYPE} from "@tarojs/taro";
import {WeappEnsureLoginStatus, WeappLoginStatus} from "@/components/LoginStatus/weapp";
import {WebEnsureLoginStatus, WebLoginStatus} from "@/components/LoginStatus/web";
import {View} from "@tarojs/components";

export const LoginStatus = () => {
  return <View>
    {Taro.getEnv() === ENV_TYPE.WEAPP ? <WeappLoginStatus /> : <WebLoginStatus />}
  </View>;
}

export const EnsureLoginStatus = () => Taro.getEnv() === ENV_TYPE.WEAPP ? <WeappEnsureLoginStatus /> :
  <WebEnsureLoginStatus />
