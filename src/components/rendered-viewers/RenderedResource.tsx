import JsonViewer from "@/components/rendered-viewers/JsonViewer";
import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import { gql } from "@apollo/client";
import { View } from "@tarojs/components";
import LinkedImage from "@/components/LinkedImage";
import { AtAvatar, AtSegmentedControl } from "taro-ui";
import { useState } from "react";

type StrapiMediaLibraryFolderViewProps = {
  attributes: {
    name: string;
    path: string;
    files: {
      data: [
        {
          attributes: {
            name: string;
            mime: string;
            previewUrl: string;
            url: string;
          };
        },
      ];
    };
  };
};

const StrapiMediaLibraryFolderView = ({
  data,
}: {
  data: StrapiMediaLibraryFolderViewProps;
}) => {
  const [currentView, setCurrentView] = useState(0);

  return (
    <View>
      <View>{data?.attributes?.name}</View>
      <AtSegmentedControl
        current={currentView}
        values={["小图总览", "大图平铺"]}
        onClick={(value) => {
          setCurrentView(value);
        }}
      />
      {currentView === 0 && (
        <View>
          {data.attributes.files.data.map((file, index) => (
            <View
              key={`small-view-${index}-${file.attributes.url}`}
              style={{ display: "inline-block", margin: "auto 5px" }}
            >
              <AtAvatar image={file.attributes.previewUrl} />
            </View>
          ))}
        </View>
      )}
      {currentView === 1 && (
        <View>
          {data.attributes.files.data.map((file, index) => (
            <View
              key={`${index}-${file.attributes.url}`}
              style={{ display: "inline-block", margin: "auto 5px" }}
            >
              <LinkedImage src={file.attributes.url} />
            </View>
          ))}
        </View>
      )}
    </View>
  );
};

const RenderedResource = ({ data }) => {
  if (data?.uploadFolder?.__typename === "UploadFolderEntityResponse") {
    return <StrapiMediaLibraryFolderView data={data?.uploadFolder?.data} />;
  }

  return <JsonViewer json={data} />;
};

export default ({ graphqlQuery: graphqlQueryJson }) => {
  if (!graphqlQueryJson) {
    return <View></View>;
  }

  const parsed = JSON.parse(graphqlQueryJson);
  const graphqlQuery = parsed.query;

  return withGraphqlQuery(RenderedResource)({
    graphqlQuery: gql`
      ${graphqlQuery}
    `,
    graphqlQueryOptions: { variables: parsed.variables },
  });
};
