import {AtForm, AtInput, AtSegmentedControl, AtTextarea} from "taro-ui";
import {Label, View} from "@tarojs/components";
import {useState} from "react";
import {isJson} from "@/common/json-extensions";

const JsonViewer = ({json, translations}: { json: string | object, translations?: object }) => {
  const isJsonString = !!json && typeof json === 'string' && isJson(json);
  return <AtForm>
    {isJsonString && <JsonViewerWrapper json={JSON.parse(json)} initialViewMode={JsonViewerViewMode.parsed}/>}

    {(!json || (!isJsonString && typeof json === 'string')) &&
      <View>{`${translations ? (translations[json] ?? json) : json}`}</View>}
    {
      !!json && typeof json === 'object' && Object.keys(json).map((key, index) => {
        if (!!json[key] && typeof json[key] === 'object') {
          return <View key={`object-${key}-${index}`}>
            <Label>{key} 开始：</Label>
            <JsonViewer json={json[key]} translations={translations}/>
            <Label>{key} 结束。</Label>
          </View>
        }

        if (!!json[key] && typeof json[key] === 'string' && isJson(json[key]) && !global.isFinite(json[key])) {
          return <View key={`string-${key}-${index}`}>
            <Label>{key} 开始：</Label>
            <JsonViewer json={JSON.parse(json[key])} translations={translations}/>
            <Label>{key} 结束。</Label>
          </View>
        }

        if (!!json[key] && typeof json[key] === 'string' && json[key].length > 20) {
          return <View key={`long-string-${key}-${index}`}>
            <Label>{key}:</Label>
            <AtTextarea key={key} placeholder={key} value={json[key]} disabled
                        onChange={() => {
                        }}
            />
          </View>
        }

        return <View key={`other-${key}-${index}`}>
          <AtInput key={key} name={key} title={translations ? (translations[key] ?? key) : key} type='text'
                   placeholder={key} value={json[key]} disabled
                   onChange={() => {
                   }}
          />
        </View>
      })
    }
  </AtForm>
}

export default JsonViewer

export enum JsonViewerViewMode {
  parsed = 0,
  raw = 1
}

export const JsonViewerWrapper = ({json, initialViewMode = JsonViewerViewMode.parsed, translations}: {
  json: object,
  initialViewMode: JsonViewerViewMode,
  translations?: object
}) => {
  const [currentView, setCurrentView] = useState(initialViewMode);
  return <View>
    <AtSegmentedControl current={currentView} values={['解析视图', '原始文本']} onClick={(value) => {
      setCurrentView(value);
    }}/>
    {
      currentView === JsonViewerViewMode.parsed && <JsonViewer json={json} translations={translations}/>
    }
    {
      currentView === JsonViewerViewMode.raw &&
      <AtTextarea maxLength={9999} height={800} disabled value={JSON.stringify(json, undefined, 4)}
                  onChange={() => {
                  }}
      />
    }
  </View>
}
