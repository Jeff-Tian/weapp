import {isGraphQLQuery} from "@/common/json-extensions";
import JsonViewer from "@/components/rendered-viewers/JsonViewer";
import RenderedResource from "@/components/rendered-viewers/RenderedResource";

export const RenderedViewer = ({content}) => {
  return isGraphQLQuery(content) ? <RenderedResource graphqlQuery={content}/> : <JsonViewer json={content}/>
}
