import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";
import {gql} from "@apollo/client";
import {ScrollView, View} from "@tarojs/components";
import {AtButton, AtCheckbox, AtFloatLayout} from "taro-ui";
import {authingAppId} from "@/common/constants";
import {useContext, useState} from "react";
import {client} from "@/apollo-client";
import Taro from "@tarojs/taro";
import {RefreshContext} from "@/contexts/refresh-context";

const UserList = ({data, extra}) => {
  const {refresh} = useContext(RefreshContext);

  const [checkedList, setCheckedList] = useState([]);

  const setupOnChange = (value) => {
    setCheckedList(value);
  };

  const setupConfirmAuthorizeTo = async () => {
    const authorizeGraphQL = gql`
      mutation AuthorizeResource($resourceCode: String!, $userIdList: [String!]!, $actions: [ResourceActionInput!]) {
        authorizeResource(code: $resourceCode, userIdList: $userIdList, actions: $actions) {
          success
        }
      }
    `;

    const result = await client.mutate({
      mutation: authorizeGraphQL,
      variables: {
        resourceCode: extra.resourceCode,
        userIdList: checkedList,
        actions: [{name: `read`, description: `read`}]
      }
    });

    refresh();
    console.log("auth result = ", result);
    await Taro.showToast({
      title: result.data.authorizeResource.success ? '授权成功' : '授权失败',
      icon: result.data.authorizeResource.success ? 'success' : 'error',
      duration: 2000
    });
  };

  return <View>
    <AtFloatLayout isOpened={extra?.isOpened} title='选择用户以允许其访问本资源' onClose={() => {
      extra.setIsOpened(false);
    }}
    >
      <ScrollView scrollY style={{height: '400px'}}>
        <AtCheckbox
          options={data?.friendListByPage.list.map((user) => ({
            value: user.userId,
            label: `${user.nickname ?? ''} ${user.name ?? ''} ${user.username ?? ''}` ?? user.userId,
            desc: `联系方式： ${user.email ?? ''} ${user.phone ?? ''}，注册于： ${user.createdAt}，登录次数： ${user.loginsCount}，最后登录： ${user.lastLogin}`
          }))}
          selectedList={checkedList}
          onChange={setupOnChange}
        />
      </ScrollView>
      <AtButton type='primary' onClick={setupConfirmAuthorizeTo}>确认授权</AtButton>
    </AtFloatLayout>
  </View>
}

const SelectableUserList = withGraphqlQuery(UserList, gql`
  query GetFriendList($page: Int!, $pageSize: Int!) {
    friendListByPage (appId: "${authingAppId}", page: $page, pageSize: $pageSize) {
      list {
        name
        email
        nickname
        userId
        username
        lastLogin
        phone
        photo
        loginsCount
        createdAt
      }
      totalCount
    }
  }
`, {
  variables: {
    page: 1,
    pageSize: 5
  }
})

export default ({resourceCode, isOpened, setIsOpened}) => <SelectableUserList
  extra={{resourceCode, isOpened, setIsOpened}}
/>
