import SinglePageLayout from "@/layout/single-page-layout";
import { View } from "@tarojs/components";
import Taro from "@tarojs/taro";

interface WithEnvProps<TData> {
  data?: TData | null | undefined;
}

interface HOCProps<TData> extends WithEnvProps<TData> {
  envTypes: TaroGeneral.ENV_TYPE[];
}

const withEnv = <TData extends any = any>(
  WrappedComponent: React.ComponentType<WithEnvProps<TData>>,
  ...envTypes: TaroGeneral.ENV_TYPE[]
) => {
  return ({ data }: HOCProps<TData>) => {
    if (!envTypes.includes(Taro.getEnv())) {
      return (
        <SinglePageLayout>
          <View>仅 {envTypes.join(", ")} 支持</View>
        </SinglePageLayout>
      );
    }

    return <WrappedComponent data={data} />;
  };
};

export default withEnv;
