import React, {useContext, useEffect} from "react";
import {getUserInfo} from "@/common/login";
import SinglePageLayout from "@/layout/single-page-layout";
import {View} from "@tarojs/components";
import {UserInfoContext} from "@/contexts/user-info-context";

interface WithProtectedProps<TData> {
  data?: TData | null | undefined;
}

interface HOCProps<TData> extends WithProtectedProps<TData> {
}

export const trustedUserIds = [
  "6204de0a49c31874b97d470b",
  "6244f2c0016cd83f2177f941",
  "6389d4831db16e541dc943a3",
];

const withProtected =
  <TData extends any = any>(
    WrappedComponent: React.ComponentType<WithProtectedProps<TData>>,
  ) =>
    ({data}: HOCProps<TData>) => {
      const [userInfo, setUserInfo] = useContext(UserInfoContext);

      useEffect(() => {
        getUserInfo().then(setUserInfo);
      }, []);

      if (!userInfo || !trustedUserIds.includes(userInfo.id)) {
        console.log("userInfo", userInfo);

        return (
          <SinglePageLayout>
            <View>正在建设中……</View>
          </SinglePageLayout>
        );
      }

      return <WrappedComponent data={data}/>;
    };

export default withProtected;
