import React from "react";
import {Banner} from "@/components/HomePageExtra/HomePageExtra";
import {BlogList} from "@/components/blog-list/BlogList";
import {AboutMe} from "@/components/HomePageExtra/about-me";
import {View} from "@tarojs/components";
import "../../components/yuque/article.styl";
import HardwayLayout from "../../layout/hardway-layout";
import {AtNoticebar} from "taro-ui";

const YuQue: React.FC = () => {
  return (
    <HardwayLayout>
      <Banner/>
      <View className='at-article'>
        <View className='at-article__h1'>
          关于我，“哈德韦”的自我介绍
        </View>
        <AtNoticebar icon='volume-plus'>
          以下是我，本小程序作者的个人介绍。
        </AtNoticebar>
      </View>
      <View className='at-article__content'>
        <AboutMe/>
      </View>
      <BlogList/>
    </HardwayLayout>
  );
};

export default YuQue;

definePageConfig({
  navigationBarTitleText: "哈德韦的个人小程序",
  enableShareAppMessage: true,
  enableShareTimeline: true,
  enablePullDownRefresh: true,
  onReachBottomDistance: 100,
});
