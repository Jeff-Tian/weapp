import { AtActionSheet, AtActionSheetItem } from "taro-ui"
import Taro, { ENV_TYPE } from "@tarojs/taro"
import React, { useState } from "react"
import { FabContext } from "@/contexts/fab-context"
import YuqueArticle from "@/components/yuque/article"
import assert from "assert";
import { gql } from "@apollo/client";
import HardwayLayout from "../../layout/hardway-layout"

const YuQueArticlePage: React.FC = () => {

  const params = Taro.getCurrentInstance()?.router?.params
  console.log('params = ', params);
  assert.ok(params, "本页必须传递参数！")

  const { id, slug } = params

  assert.ok(id || slug, "本页必须传递 id 或 slug 参数！")

  const YUQUE_BLOG = id ? gql`
    query {
      yuque (id: "${id}") {
        id
        title
        description
        word_count
        created_at
        cover
        body
        body_html
      }
    }
  ` : gql`
    query {
      yuque (slug: "${slug}") {
        id
        title
        description
        word_count
        created_at
        cover
        body
        body_html
      }
    }
  `

  const [fabData, setFabData] = useState({ html: '', markdown: '' });

  const [showActionSheet, setShowActionSheet] = useState(false);

  const FabActionSheet = ({ html, markdown }) => <AtActionSheet isOpened={showActionSheet}
    title='请选择复制正文的方式：'
  >
    <AtActionSheetItem
      onClick={() => {
        Taro.setClipboardData({ data: markdown });
        setShowActionSheet(false);
      }}
    >拷贝正文（markdown）</AtActionSheetItem>
    <AtActionSheetItem onClick={() => {
      Taro.setClipboardData({ data: html });
      setShowActionSheet(false);
    }}
    >拷贝正文（富文本）</AtActionSheetItem>
  </AtActionSheet>

  if (ENV_TYPE.WEB !== Taro.getEnv() && (id === '179639985' || slug === 'dgmi30fu7dl3t01l')) {
    return <FabContext.Provider value={{
      FabComponent: FabActionSheet, FabOnClick: () => setShowActionSheet(!showActionSheet),
      FabData: fabData
    }}
    >
      <HardwayLayout>
        <YuqueArticle action={setFabData} data={{ yuque: { title: 'React Context 的妙用', body_html: 'React Context 虽好，但是用多了会造成一种 React Context 地狱的效果，那有没有解决办法呢？敬请期待下篇文章分享……' } }} />
      </HardwayLayout>
    </FabContext.Provider>
  }

  return <FabContext.Provider value={{
    FabComponent: FabActionSheet, FabOnClick: () => setShowActionSheet(!showActionSheet),
    FabData: fabData
  }}
  >
    <HardwayLayout>
      <YuqueArticle action={setFabData} graphqlQuery={YUQUE_BLOG} />
    </HardwayLayout>
  </FabContext.Provider>
}

export default YuQueArticlePage
