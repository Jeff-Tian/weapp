import Taro from "@tarojs/taro"
import GITHUB_README from "@/components/github/readme";
import SinglePageLayout from "@/layout/single-page-layout";

const ReadmePage = () => {
  const {owner = 'Jeff-Tian', name = 'tic-tac-toe-ai'} = Taro.getCurrentInstance()?.router?.params ?? {owner: 'Jeff-Tian', name: 'tic-tac-toe-ai'};

  return <SinglePageLayout>
    <GITHUB_README graphqlQueryOptions={{variables: {owner, name}}}/>
  </SinglePageLayout>
}

export default ReadmePage;

definePageConfig({
  navigationBarTitleText: 'GitHub 仓库 README.md',
  enableShareAppMessage: true,
  enableShareTimeline: true
})
