import HardwayLayout from "@/layout/hardway-layout";
import {View} from "@tarojs/components";

const Instructions = () => {
  return <HardwayLayout>
    <View className='at-article'>
      <View className='at-article__h1'>本小程序使用说明</View>
      <View className='at-article__content'>
        <View className='at-article__section'>
          <View className='at-article__h2'>特殊手势</View>
          <View className='at-article__h3'>
            单击标题
          </View>
          <View className='at-article__p'>
            单击标题会回到本小程序的首页。
          </View>
          <View className='at-article__h3'>
            双击标题
          </View>
          <View className='at-article__h4'>
            当滚动到下方时双击
          </View>
          <View className='at-article__p'>
            当页面比较长，已经滚动到下方时，双击标题会快速回到页面顶部。
          </View>
          <View className='at-article__h4'>
            已经位于页面顶部时双击
          </View>
          <View className='at-article__p'>
            当页面已经位于顶部时，双击标题会刷新页面。
          </View>
        </View>
        <View className='at-article__section'>
          <View className='at-article__h2'>浮动按钮的使用</View>
          <View className='at-article__h3'>快速跳转到深链接</View>
          <View className='at-article__p'>
            如果复制了一个深链接，可以点击浮动按钮，即可快速跳转到该深链接。目前支持的有我的语雀文章链接，假如复制了一个语雀文章链接到剪贴板时，点击浮动按钮即可快速跳转到该文章。
          </View>
          <View className='at-article__h3'>复制博客文章内容</View>
          <View className='at-article__p'>
            如果进入了一个博客文章页面，可以点击浮动按钮，即可复制该文章的内容到剪贴板。复制时会弹出一个提示框，可以在该提示框中选择是复制纯文本还是复制带有格式的 HTML。
          </View>
        </View>
      </View>
    </View>
  </HardwayLayout>
}

export default Instructions;

definePageConfig({
  navigationBarTitleText: '本小程序使用说明',
  enableShareAppMessage: true,
  enableShareTimeline: true,
  backgroundColor: '#d45645',
  backgroundColorTop: '#d45645',
  backgroundColorBottom: '#d45645',
  navigationBarBackgroundColor: '#d45645',
  enablePullDownRefresh: true,
  onReachBottomDistance: 50
})

