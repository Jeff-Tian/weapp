import {View} from "@tarojs/components";
import {GITHUB_STATS} from "@/api/github";
import GitHubCalendar from "@/components/github/calendar";
import SinglePageLayout from "@/layout/single-page-layout";
import {JsonViewerViewMode, JsonViewerWrapper} from "@/components/rendered-viewers/JsonViewer";
import GitHubTimeline from "@/components/github/timeline";
import withGraphqlQuery from "@/components/higher-orders/with-graphql-query";

const GitHubProfile = ({data}) => {
  const {contributionCalendar, ...rest} = data?.viewer?.contributionsCollection ?? {};

  return (
    <SinglePageLayout bgColor='white' title='GitHub 活动'>
      <GitHubCalendar/>
      <View>在过去的一年中，{data?.viewer?.login} 一共提交了 {contributionCalendar?.totalContributions} 次</View>
      <JsonViewerWrapper json={rest} initialViewMode={JsonViewerViewMode.parsed} translations={{
        '__typename': '类型',
        'ContributionsCollection': '#',
        'totalIssueContributions': '总共贡献的问题数',
        'totalCommitContributions': '总共贡献的提交数',
        'totalPullRequestContributions': '总共贡献的拉取请求数',
        'totalPullRequestReviewContributions': '总共贡献的拉取请求审查数',
        'totalRepositoryContributions': '总共贡献的仓库数',
        'totalRepositoriesWithContributedCommits': '总共贡献的有提交的仓库数',
        'totalRepositoriesWithContributedIssues': '总共贡献的有问题的仓库数',
        'totalRepositoriesWithContributedPullRequestReviews': '总共贡献的有拉取请求审查的仓库数',
        'totalRepositoriesWithContributedPullRequests': '总共贡献的有拉取请求的仓库数',
      }}
      />
      <GitHubTimeline/>
    </SinglePageLayout>
  )
}

export default withGraphqlQuery(GitHubProfile, GITHUB_STATS);

definePageConfig({
  navigationBarTitleText: 'Jeff Tian\'s GitHub Profile',
  enableShareAppMessage: true,
  enableShareTimeline: true,
  backgroundColor: '#d45645',
  backgroundColorTop: '#d45645',
  backgroundColorBottom: '#d45645',
  navigationBarBackgroundColor: '#d45645',
  enablePullDownRefresh: true,
  onReachBottomDistance: 50
})
