import {View} from "@tarojs/components"
import {gql} from "@apollo/client";
import Taro, {ENV_TYPE} from '@tarojs/taro';
import {useEffect, useState} from "react";
import cssToJS from "transform-css-to-js"
import HardwayLayout from "@/layout/hardway-layout";
import {AtButton, AtDivider} from "taro-ui";
import GITHUB_README from "@/components/github/readme";

import './tictactoe.styl'
import {DynamicContent} from "./dynamic-content";

const transformRequest = gql`query transformTsx {
  transform (url: "https://raw.githubusercontent.com/Jeff-Tian/TicTacToeTs/main/src/GameAI.tsx?a=bcdefg", extra: "; ReactDOM.render(<Game />, document.getElementById('root'))") {
    text
  }
}`


const TicTacToe = () => {
  const [css, setCss] = useState('')

  useEffect(() => {
    Taro.request({url: `https://uniheart.pa-ca.me/proxy?format=json&url=${encodeURIComponent(`https://raw.githubusercontent.com/Jeff-Tian/TicTacToeTs/main/src/index.css`)}`}).then(({data}) => {
      console.log('data = ', data)
      setCss(data.raw)
      const reactNativeCompatibleCSS = cssToJS(data.raw)
      console.log('rcss = ', reactNativeCompatibleCSS)
    })
  }, [])

  return <HardwayLayout>
    <View>
      <View>你先走哦</View>
      <DynamicContent gql={transformRequest} />
    </View>
    <AtDivider />
    {
      ENV_TYPE.WEAPP === Taro.getEnv() ? <View></View> :
        <View>
          <GITHUB_README />
          <AtButton type='primary'
            onClick={() => Taro.navigateTo({url: '/pages/subpages/video/detail?zVideoId=1762911635101216768'})}
          >开发实录</AtButton>
        </View>
    }
  </HardwayLayout>;
}

export default TicTacToe

definePageConfig({
  navigationBarTitleText: '解压井字棋',
  enableShareAppMessage: true,
  enableShareTimeline: true
});
