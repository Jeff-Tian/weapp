import SinglePageLayout from "@/layout/single-page-layout";
import {LoginStatus} from "@/components/LoginStatus";
import {useContext} from "react";
import {AppContext, AppNameEnum} from "@/contexts/app-context";
import SimpleLayout from "@/layout/simple-layout";
import MyClipboard from "@/components/my-clipboard/MyClipboard";
import MyBluetoothDevices from "@/components/bluetooth/MyBluetoothDevices";
import Taro, {ENV_TYPE} from "@tarojs/taro";

const Authing = () => {
  const {appName} = useContext(AppContext);

  const Layout = appName === AppNameEnum.brickverse ? SimpleLayout : SinglePageLayout;

  return <Layout>
    <LoginStatus />
    <MyClipboard />
    {
      Taro.getEnv() !== ENV_TYPE.WEB &&
      <MyBluetoothDevices />
    }
  </Layout>
}

export default Authing
