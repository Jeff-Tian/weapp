import {View} from "@tarojs/components";
import Taro, {useDidHide, useDidShow} from "@tarojs/taro";
import ResourceDetailWithCode from "@/components/resources/ResourceDetail";
import ResourceAuthorizedUsers from "@/components/resources/ResourceAuthorizedUsers";
import UserList from "@/components/UserList";
import {useContext, useState} from "react";
import HardwayLayout from "@/layout/hardway-layout";
import {ShareTokenByQueryStringContext} from "@/contexts/share-token";
import {EnsureLoginStatus} from "@/components/LoginStatus";
import {getResourceNameWithoutOwnerUserIdPrefix} from "@/common/resources";
import {UserRolesContext} from "@/contexts/user-info-context";
import {RoleTags} from "@/components/roles/roleTags";

const Resource = () => {
  const [roles] = useContext(UserRolesContext);

  const [_, setShareTokenByQueryString] = useContext(
    ShareTokenByQueryStringContext,
  );

  useDidShow(() => {
    setShareTokenByQueryString(true);
  });

  useDidHide(() => {
    setShareTokenByQueryString(false);
  });

  useDidShow(()=>{
    console.log('roles:', roles)
    const trusted = roles.map(r => r.code).includes('wmp-trusted-users');

    if (trusted) {
      Taro.showShareMenu({withShareTicket: true})
    }
  })

  const params = Taro.getCurrentInstance().router?.params;

  const [isUserListOpen, setIsUserListOpen] = useState<boolean>(false);

  if (!params?.code) {
    return <View>正在建设中……</View>;
  }

  Taro.setNavigationBarTitle({
    title: getResourceNameWithoutOwnerUserIdPrefix(params.code),
  });

  return (
    <HardwayLayout>
      <EnsureLoginStatus/>
      <ResourceDetailWithCode code={params.code}/>
      <ResourceAuthorizedUsers
        resourceCode={params.code}
        setIsUserListOpen={setIsUserListOpen}
      />
      <UserList
        resourceCode={params.code}
        isOpened={isUserListOpen}
        setIsOpened={setIsUserListOpen}
      />

      <RoleTags />
    </HardwayLayout>
  );
};

export default Resource;

definePageConfig({
  navigationBarTitleText: "正在建设中……",
  enableShareAppMessage: false,
  enableShareTimeline: false,
  backgroundColor: "#d45645",
  backgroundColorTop: "#d45645",
  backgroundColorBottom: "#d45645",
  navigationBarBackgroundColor: "#d45645",
});
