import {createResource} from "@/api/user-service";
import HardwayLayout from "@/layout/hardway-layout";
import Taro, {useDidShow} from "@tarojs/taro";
import {AtButton, AtForm, AtInput} from "taro-ui";
import ResourceList from "@/components/resources/ResourceList";
import {useContext, useState} from "react";
import {RefreshContext} from "@/contexts/refresh-context";
import {RoleTags} from "@/components/roles/roleTags";
import {UserRolesContext} from "@/contexts/user-info-context";

const Resources = () => {
  const {refresh} = useContext(RefreshContext);
  const [roles] = useContext(UserRolesContext);

  const createResourceButtonClicked = async (): Promise<void> => {
    Taro.showModal({
      title: "创建备忘录",
      content: `即将创建名为 ${resourceName} 的备忘录`,
      showCancel: true,
      cancelText: "取消",
      success: async (res) => {
        if (res.confirm) {
          const result = await createResource(resourceName);

          refresh();
          Taro.showModal({
            title: "创建成功！",
            content: "结果： " + JSON.stringify(result),
            showCancel: false,
          });
        }
      },
    });
  };

  const [resourceName, setResourceName] = useState("");

  useDidShow(()=>{
    console.log('roles:', roles)
    const trusted = roles.map(r => r.code).includes('wmp-trusted-users');

    if (trusted) {
      Taro.showShareMenu({withShareTicket: true})
    }
  })

  return (
    <HardwayLayout>
      <ResourceList/>

      <AtForm>
        <AtInput
          name="resource-name"
          title="起个名字："
          type="text"
          placeholder="请输入备忘录名称"
          value={resourceName}
          onChange={(value) => setResourceName(value.toString())}
        ></AtInput>
        <AtButton
          type="primary"
          formType="submit"
          onClick={createResourceButtonClicked}
        >
          创建备忘录
        </AtButton>
      </AtForm>
      <RoleTags />
    </HardwayLayout>
  );
};

export default Resources;

definePageConfig({
  navigationBarTitleText: "备忘录",
  enableShareAppMessage: false,
  enableShareTimeline: false,
  backgroundColor: "#d45645",
  backgroundColorTop: "#d45645",
  backgroundColorBottom: "#d45645",
  navigationBarBackgroundColor: "#d45645",
});
