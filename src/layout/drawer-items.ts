import Taro from "@tarojs/taro";
import { setupNavigate } from "@/functions/redirect";

export const drawerItems = new Map<string, () => void>([
  ["备忘录", setupNavigate("/pages/subpages/auth/resources")],
  ["解压井字棋", setupNavigate("/pages/subpages/tictactoe/ai")],
  ["关于", setupNavigate("/pages/subpages/about/index")],
  ["使用说明", setupNavigate("/pages/subpages/about/instructions")],
  // ['哈德韦的朋友们', async () => Taro.navigateTo({url: '/pages/subpages/friends/list'})],
  // ['微信表情', async () => Taro.navigateTo({url: '/pages/subpages/sticker/index'})],
  // ['测试', async () => Taro.navigateTo({url: '/pages/subpages/test/index'})],
]);

if (Taro.getEnv() === Taro.ENV_TYPE.WEB) {
  drawerItems.set(
    "brickverse",
    setupNavigate("/pages/subpages/brickverse/index"),
  );
  drawerItems.set("换脸", setupNavigate("/pages/subpages/face-swap/index"));
  drawerItems.set("向我咨询", async () =>
    window.open("https://www.zhihu.com/consult/people/1073548674713423872"),
  );
  drawerItems.set(
    "支持哈德韦",
    setupNavigate("/pages/subpages/order/support-me"),
  );
}

export const onDrawerItemClick = (index) => [...drawerItems.values()][index]();
