import {OfficialAccount, View} from "@tarojs/components"
import {HighLevel} from "@/layout/components/high-level";
import Taro, {ENV_TYPE} from "@tarojs/taro";
import {Fab} from "@/layout/components/fab/Fab";
import '../components/rich-modal.styl'


const HardwayLayout = ({children}) => {
  return <View>
    <HighLevel />
    <View style={{minHeight: '1000px', paddingTop: '1em'}}>
      {Taro.getEnv() === ENV_TYPE.WEAPP && <OfficialAccount />}
      {children}
    </View>
    <Fab />
  </View>
}

export default HardwayLayout
