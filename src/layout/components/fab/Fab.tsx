import {View} from "@tarojs/components";
import {handleClipboard} from "@/functions/clipboard";
import {AtAvatar} from "taro-ui";
import {useContext} from "react";
import {FabContext} from "@/contexts/fab-context";
import Taro from "@tarojs/taro";

import './fab.styl';

export const Fab = () => {
  const {FabComponent, FabOnClick, FabData} = useContext(FabContext);

  const handleFabClick = () => {
    Taro.vibrateShort({
      type: 'light'
    })
    
    if (typeof FabOnClick === 'function') {
      FabOnClick();
    } else {
      return handleClipboard()
    }
  }

  return <View className='fab-area' onClick={handleFabClick}>
    <AtAvatar circle image='https://avatars.githubusercontent.com/u/3367820?v=4'></AtAvatar>

    <FabComponent {...FabData} />
  </View>;
}
