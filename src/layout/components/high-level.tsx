import {useContext, useEffect, useState} from "react";
import Taro from "@tarojs/taro";
import {View} from "@tarojs/components";
import {AtDrawer, AtNavBar} from "taro-ui";
import {drawerItems, onDrawerItemClick} from "@/layout/drawer-items";
import {getUserInfo} from "@/common/login";
import {copyCurrentPagePath} from "@/functions/current-page";
import {naiveErrorHandler} from "@/functions/naiveErrorHandler";
import {RefreshContext} from "@/contexts/refresh-context";
import {CurrentClipboardContext} from "@/contexts/clipboard-context";
import {setupSmartAction} from "@/functions/smartAction";
import {ShareTokenByQueryStringContext} from "@/contexts/share-token";
import {UserInfoContext} from "@/contexts/user-info-context";

export const HighLevel = ({title}: { title?: string }) => {
  const {refresh} = useContext(RefreshContext);
  const [showDrawer, setShowDrawer] = useState(false);

  const [userInfo, setUserInfo] = useContext(UserInfoContext);

  const [, setCurrentClipboard] = useContext(CurrentClipboardContext);
  const [shareTokenByQueryString] = useContext(ShareTokenByQueryStringContext);
  useEffect(() => {
    getUserInfo().then(setUserInfo).catch(naiveErrorHandler);
  }, []);

  const setupUserProfileOnClick = () => {
    Taro.navigateTo({url: "/pages/subpages/auth/authing"});
  };

  const setupDrawerToggle = () => {
    setShowDrawer(true);
  };

  return (
    <View style={{marginBottom: '64px'}}>
      <AtNavBar
        onClickRgIconSt={setupDrawerToggle}
        onClickRgIconNd={setupUserProfileOnClick}
        onClickLeftIcon={() => copyCurrentPagePath(setCurrentClipboard, shareTokenByQueryString)}
        color='#000'
        title={title ?? '哈德韦'}
        leftText=''
        leftIconType='link'
        rightFirstIconType='bullet-list'
        rightSecondIconType={
          userInfo ? {value: "user", color: "blue"} : "user"
        }
        onClickTitle={setupSmartAction(refresh)}
        fixed
        border
      />

      <AtDrawer
        show={showDrawer}
        mask
        right
        onClose={() => setShowDrawer(false)}
        items={[...drawerItems.keys()]}
        onItemClick={onDrawerItemClick}
      />
    </View>
  );
};
