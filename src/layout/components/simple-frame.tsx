import {useContext, useEffect} from "react";
import Taro from "@tarojs/taro";
import {View} from "@tarojs/components";
import {AtNavBar} from "taro-ui";
import {getUserInfo} from "@/common/login";
import {copyCurrentPagePath} from "@/functions/current-page";
import {naiveErrorHandler} from "@/functions/naiveErrorHandler";
import {setupSmartAction} from "@/functions/smartAction";
import usePageRefresh from "@/hooks/use-page-refresh";
import {CurrentClipboardContext} from "@/contexts/clipboard-context";
import {ShareTokenByQueryStringContext} from "@/contexts/share-token";
import {UserInfoContext} from "@/contexts/user-info-context";

export const SimpleFrame = () => {
  const {refresh} = usePageRefresh()

  const [userInfo, setUserInfo] = useContext(UserInfoContext);

  useEffect(() => {
    getUserInfo().then(setUserInfo).catch(naiveErrorHandler);
  }, []);

  const [, setCurrentClipboard] = useContext(CurrentClipboardContext);
  const [shareTokenByQueryString] = useContext(ShareTokenByQueryStringContext);
  return (
    <View>
      <AtNavBar
        onClickRgIconSt={() => {
          console.log('hello!')
        }}
        onClickRgIconNd={() => {
          Taro.navigateTo({url: "/pages/subpages/auth/authing"});
        }}
        onClickLeftIcon={() => copyCurrentPagePath(setCurrentClipboard, shareTokenByQueryString)}
        color='#000'
        title='Brickverse'
        leftText=''
        leftIconType='link'
        rightFirstIconType='bullet-list'
        rightSecondIconType={
          userInfo ? {value: "user", color: "blue"} : "user"
        }
        onClickTitle={setupSmartAction(refresh, () => Taro.navigateTo({url: "/pages/subpages/brickverse/index"}))}
      />
    </View>
  );
};
