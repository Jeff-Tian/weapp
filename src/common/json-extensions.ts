export const isJson = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }

  return true;
}

export const isGraphQLQuery = (str) => {
  if (typeof str !== 'string') {
    return false;
  }
  return (str ?? '').trim().startsWith('{"query":');
}
