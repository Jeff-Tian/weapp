export const getResourceNameWithoutOwnerUserIdPrefix = (resourceName: string) =>
  resourceName?.replace(/^[0-9a-f]{24}-/, "");
