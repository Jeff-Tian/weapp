import React from "react";

export const RefreshContext = React.createContext({
  refresh: () => {
  },
  register: () => {
  },
  unregister: () => {
  }
} as any);
