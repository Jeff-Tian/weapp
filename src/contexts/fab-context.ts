import {createContext} from "react";

interface FabContextProp {
  FabComponent: any
  FabOnClick?: Function
  FabData: any
}

export const FabContext = createContext<FabContextProp>({
  FabComponent: (props: any): any => null,
  FabOnClick: undefined,
  FabData: {},
})
