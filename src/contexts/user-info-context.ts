import {createContext, Dispatch, SetStateAction} from 'react';
import {User} from "@authing/guard-react";

export const UserInfoContext = createContext<[User | null, Dispatch<SetStateAction<User | null>>]>([null, () => {
}]);

export const UserRolesContext = createContext<[string[], Dispatch<SetStateAction<string[]>>]>([[], () => {
}]);
