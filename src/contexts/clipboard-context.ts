import {Dispatch, SetStateAction, createContext} from "react";

export const CurrentClipboardContext = createContext<[string, Dispatch<SetStateAction<string>>]>(['', () => {
}]);

export const CurrentCloudboardContext = createContext<[undefined, Dispatch<SetStateAction<any>>]>([undefined, () => {
}]);
