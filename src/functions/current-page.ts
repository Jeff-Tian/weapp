import Taro from "@tarojs/taro";
import qs from "querystring";
import { getToken } from "@/common/token";
import packageJson from "../../package.json";

export const getCurrentPagePathAndQueryString = async (
  router = Taro.getCurrentInstance().router,
  shareTokenByQueryString = false,
) => {
  if (!router) {
    return ``;
  }

  const { params, path } = router;
  const pathWithoutQuery = path.split("?")[0];
  const { $taroTimestamp, ...query } = params;

  // 这里原本判断了是否是网页环境，如果是网页环境，就会通过 hash 加上 token。
  // 但是实测发现，hash 会在真实的小程序环境中被忽略，所以这里不再判断环境，并且总是通过 querystring 加上 token。
  // 这个 hash 被忽略的情况，即使通过小程序开发工具，也不存在。即开发过程中很难发现这个问题。
  // 但是上线后，在实际的小程序中就会发现这个问题了。
  const linkWithTokenAttached = `?${qs.stringify({
    ...query,
    token: shareTokenByQueryString ? await getToken() : undefined,
  })}`;

  return `${pathWithoutQuery}${Object.keys(query).length === 0 ? "" : linkWithTokenAttached}`;
};

const getCurrentPageFullUrl = async (
  router = Taro.getCurrentInstance().router,
  shareTokenByQueryString,
) => {
  const origin = document?.location?.origin ?? `https://taro.jefftian.dev`;

  return `${origin}${await getCurrentPagePathAndQueryString(router, shareTokenByQueryString)}`;
};

const copyToClipboard = (path, setCurrentClipboardCallback) => {
  Taro.setClipboardData({ data: `${path}` })
    .then(() => {
      setCurrentClipboardCallback && setCurrentClipboardCallback(path);

      return Taro.showToast({
        title: `${path} 已复制`,
        icon: "success",
      });
    })
    .catch((err) =>
      Taro.showToast({
        title: `${path} 复制失败：${err}`,
        icon: "error",
      }),
    );
};

export const copyCurrentPagePath = async (
  setCurrentClipboard,
  shareTokenByQueryString,
) => {
  copyToClipboard(
    await getCurrentPagePathAndQueryString(
      Taro.getCurrentInstance().router,
      shareTokenByQueryString,
    ),
    setCurrentClipboard,
  );

  Taro.showActionSheet({
    alertText: "复制当前页面链接",
    itemList: ["复制完整网页链接", "复制页面路径", "复制小程序文字链接"],
    success: async (res) => {
      res.tapIndex === 0 &&
        copyToClipboard(
          await getCurrentPageFullUrl(
            Taro.getCurrentInstance().router,
            shareTokenByQueryString,
          ),
          setCurrentClipboard,
        );

      res.tapIndex === 1 &&
        copyToClipboard(
          await getCurrentPagePathAndQueryString(
            Taro.getCurrentInstance().router,
            shareTokenByQueryString,
          ),
          setCurrentClipboard,
        );

      res.tapIndex === 2 &&
        copyToClipboard(`#小程序:${packageJson.name}`, setCurrentClipboard);
    },

    fail: console.error,
  });
};
