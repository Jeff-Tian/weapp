import Taro from "@tarojs/taro";
import config from "../config/config";

function handleYuQueUrl(url: string) {
  if (url.indexOf(`yuque.com`) >= 0) {
    const slug = url.split(`/`).slice(-1)[0]

    if (slug) {
      Taro.showModal({
        title: '从剪贴板里检测到语雀链接',
        content: '是否打开语雀文章？',
        success: (res) => {
          if (res.confirm) {
            Taro.navigateTo({url: `/pages/yuque/article?slug=${slug.replace(/"/g, '')}`})
          }
        }
      })
    }
  }
}

function handleDeepLink(data: string) {
  const deepLink = matchDeepLink(getAllDeepLinksFromConfig(), data)

  if (!deepLink) {
    return;
  }

  Taro.showModal({
    title: '从剪贴板里检测到页面链接',
    content: `想要跳转到 ${deepLink} 吗？`,
    success: (res) => {
      if (res.confirm) {
        Taro.navigateTo({url: deepLink})
      }
    }
  })
}

export const handleClipboard = async () => {
  if (Taro.getEnv() === Taro.ENV_TYPE.WEB) {
    const clipboardData = await navigator.clipboard.readText();
    console.log('clipboard = ', clipboardData);

    await Taro.setClipboardData({
      data: clipboardData
    })
  }

  Taro.getClipboardData({
    success: res => {
      console.log("res= ", res)
      handleYuQueUrl(res.data)
      handleDeepLink(res.data)
    }
  })
}

export function matchDeepLink(allDeepLinks: string[], linkToMatch: string) {
  const myDomains = [
    'https://taro.jefftian.dev',
    'https://taro.pa-ca.me',
    'http://localhost:10086'
  ]

  if (!linkToMatch || (!linkToMatch.startsWith('pages/') && !linkToMatch.startsWith('/pages/') && !linkToMatch.startsWith('https://') && !linkToMatch.startsWith('http://'))) {
    return null;
  }

  for (const domain of myDomains) {
    if (linkToMatch.startsWith(domain)) {
      linkToMatch = linkToMatch.replace(domain, '');
    }
  }

  if (linkToMatch.indexOf('.html') > 0) {
    linkToMatch = linkToMatch.replace('.html', '');
  }

  if (!linkToMatch.startsWith('/')) {
    linkToMatch = `/${linkToMatch}`;
  }

  for (const deepLink of allDeepLinks) {
    if (linkToMatch.indexOf(deepLink) >= 0) {
      return linkToMatch;
    }
  }

  return null;
}

export function getAllDeepLinksFromConfig() {
  return config.pages.concat(
    config.subpackages.flatMap(subpackage => subpackage.pages.map(page => `${subpackage.root}/${page}`))
  ).sort();
}
