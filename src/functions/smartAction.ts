import Taro from "@tarojs/taro";

let clickTimer: any = null;

/**
 * 单击标题栏时跳转到首页，双击时刷新或滚动到顶部
 * @param refresh
 * @param singleClickAction
 * @param doubleClickActionForOnTop
 * @param doubleClickActionForNotOnTop
 */
export const setupSmartAction = (
  refresh: () => void,
  singleClickAction = () => Taro.navigateTo({url: "/pages/yuque/index"}),
  doubleClickActionForOnTop = () => {
    refresh();

    // 如果已经在顶部，则执行刷新
    Taro.startPullDownRefresh().then(() => {
      console.log('pull down refresh started');
    }).catch(console.error).finally(() => {

    });
  },
  doubleClickActionForNotOnTop = () => Taro.pageScrollTo({
    scrollTop: 0,
    duration: 300
  })
) => () => {
  if (!clickTimer) {
    clickTimer = setTimeout(() => {
      singleClickAction();
      clickTimer = null;
    }, 800);
  } else {
    clearTimeout(clickTimer);
    clickTimer = null;

    selectActionByScrollPosition(doubleClickActionForOnTop, doubleClickActionForNotOnTop);
  }
}

const selectActionByScrollPosition = (
  onTopAction: () => void,
  notOnTopAction: () => void
) => {
  const query = Taro.createSelectorQuery();
  query.selectViewport().scrollOffset((res) => {
    // res.scrollTop 是页面在垂直方向已滚动的距离
    if (res.scrollTop === 0) {
      onTopAction();
    } else {
      notOnTopAction();
    }
  }).exec();
}
