import Taro from "@tarojs/taro";

export const redirectFor = (url) => {
  if (url.startsWith('pages/') && !url.startsWith('pages/subpages')) {
    return url.replace('pages/', 'pages/subpages/')
  }

  return null
}

export const tryRedirect = path => {
  const newPath = redirectFor(path.trimStart('/'));

  if (newPath) {
    Taro.redirectTo({url: newPath});
  } else {
    Taro.showModal({
      title: '未知页面',
      content: `未知页面：${path}`
    })
  }

  return
}

export const tryNavigateTo = path => {
  console.log('准备跳转到页面：', path)

  Taro.navigateTo({
    url: path, fail: error => {
      if (error.errMsg.includes('fail timeout')) {
        return;
      }

      console.error(`尝试跳转到 ${path} 失败!`)
      console.error(error);
    }
  });
}

export const setupNavigate = (path) => async () => tryNavigateTo(path);
