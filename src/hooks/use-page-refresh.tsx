import {useState, useCallback, Dispatch, SetStateAction} from "react";
import Taro from '@tarojs/taro';

const usePageRefresh = () => {
  const [refetchFunctions, setRefetchFunctions]: [Function[], Dispatch<SetStateAction<Function[]>>] = useState([]);

  const refresh = useCallback(() => {
    console.log('刷新中...', refetchFunctions);
    const refetchPromises = refetchFunctions.map(fn => fn());

    Promise.all(refetchPromises).then(() => {
      console.log('刷新完毕')
      Taro.stopPullDownRefresh();
    }).catch(error => {
      console.error(error);
      Taro.showToast({
        title: error.message,
        icon: "error",
        duration: 5000
      })
      Taro.stopPullDownRefresh();
    })
  }, [refetchFunctions]);

  const register = useCallback((fetching) => {
    setRefetchFunctions(prev => [...prev, fetching]);
  }, [refetchFunctions]);

  const unregister = useCallback((fetching) => {
    setRefetchFunctions(prev => prev.filter(fn => fn !== fetching));
  }, [refetchFunctions]);

  Taro.usePullDownRefresh(() => {
    console.info('用户下拉刷新');
    refresh();
    Taro.showToast({
      title: "刷新完毕"
    });
  });

  return {
    register, 
    unregister, 
    refresh,
    [Symbol.iterator]: function* () {
      yield register;
      yield unregister;
      yield refresh;
    }
  };
}

export default usePageRefresh;
