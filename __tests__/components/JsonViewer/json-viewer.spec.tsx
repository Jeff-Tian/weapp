import {render} from "@testing-library/react";
import JsonViewer, {JsonViewerViewMode, JsonViewerWrapper} from "@/components/rendered-viewers/JsonViewer";

it('renders as raw format', async () => {
  const {container} = render(
    <JsonViewer json={JSON.stringify({hello: 'world'})} />
  );

  expect(container.textContent).toBe(`解析视图原始文本hello`);
})


it('renders as parsed format', async () => {
  const {container} = render(
    <JsonViewerWrapper json={{hello: 'world'}} initialViewMode={JsonViewerViewMode.parsed} />
  );

  expect(container.textContent).toBe(`解析视图原始文本hello`);
})


it('renders as parsed format with translated strings', async () => {
  const {container} = render(
    <JsonViewerWrapper json={{hello: 'world'}} initialViewMode={JsonViewerViewMode.parsed} translations={{'hello': '你好'}} />
  );

  expect(container.textContent).toBe(`解析视图原始文本你好`);
})
