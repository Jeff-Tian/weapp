import { getAllDeepLinksFromConfig } from "@/functions/clipboard";
import { redirectFor } from "@/functions/redirect";
import { getAllDeepLinksFromDist } from "../../scripts/deeplinks";
import { compose, head, identity } from "../../scripts/helpers";

describe('deeplinks', () => {
  it('are equal', () => {
    const res = 'test';
    expect(head(res)).toEqual('t');
    expect(head(res)).toEqual(compose(head, identity)(res))
  })

  it('flats', () => {
    const a = [[1, 2], [3, 4], [5, 6]]
    expect(a.flatMap(x => x)).toStrictEqual([
      1, 2, 3, 4, 5, 6
    ])
  })

  it('redirects', () => {
    const path = 'pages/pearlsplus/1_6_1'
    const redirectsTo = redirectFor(path);
    expect(redirectsTo).toEqual('pages/subpages/pearlsplus/1_6_1');
  })

  it('get all the deeplink pages', () => {
    const allDeepLinksFromDist = getAllDeepLinksFromDist();

    getAllDeepLinksFromConfig().forEach(x => {
      expect(allDeepLinksFromDist).toContain(x);
    });
  })
})
