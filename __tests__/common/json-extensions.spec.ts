import {isGraphQLQuery, isJson} from "@/common/json-extensions";

it('is a json', async () => {
  // arrange
  const str = "{}";

  // act
  const result = isJson(str);

  // assert
  expect(result).toBe(true);
})

it("is Not a json", async () => {
  // arrange
  const str = "hello";

  // act
  const result = isJson(str);

  // assert
  expect(result).toBe(false);
})

it('is a GraphQL query', async () => {
  // arrange
  const str = "{\"query\": {}}";

  // act
  const result = isGraphQLQuery(str);

  // assert
  expect(result).toBe(true);
})

it('is Not a GraphQL query', async () => {
  // arrange
  const str = "{}";

  // act
  const result = isGraphQLQuery(str);

  // assert
  expect(result).toBe(false);
})
